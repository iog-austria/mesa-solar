#!/usr/bin/env python

# code adapted from https://github.com/DexterInd/BrickPi/blob/master/Software/BrickPi_Python/Project_Examples/Thingspeak%20Temperature%20Logging/Thermometer_Thingspeak.py

#from original comment
# Joshwa
# To get started on basics of pushing data to thingspeak visit : www.australianrobotics.com.au/news/how-to-talk-to-thingspeak-with-python-a-memory-cpu-monitor 

#our comment
#sending archive data to thinkspeak, account by Kathi.
#to keep it private, API key should not be written in the source code! our repository is open source!
#be aware that this code transfers data in plain text.
#for security reasons our Raspberry Pi is in a private Wifi

import httplib, urllib
import time

time_val = time.ctime()
temp_val = raw_input()
humidity_val = raw_input()
light_resistor_val = raw_input()
current_val = raw_input()

key_file = open("../../keyfile.txt", "r")

api_key = key_file.readline()

if (api_key != ""):                                
  #params = urllib.urlencode({'time': time_val, 'temperature': temp_val, 'humidity': humidity_val, 'light_resistor':light_resistor_val, 'current':current_val, 'api_key':api_key})     # use your API key generated in the thingspeak channels for the value of 'key'
  # temp is the data you will be sending to the thingspeak channel for plotting the graph. You can add more than one channel and plot more graphs
  params = urllib.urlencode({'field1': time_val,'field2': temp_val, 'field3': humidity_val, 'field4': light_resistor_val, 'field5': current_val,'api_key':api_key}) 
  headers = {"Content-typZZe": "application/x-www-form-urlencoded","Accept": "text/plain"}
  conn = httplib.HTTPSConnection("api.thingspeak.com:443")                
  cUrl = "/update?"+params # test werte ... bitte loeschen
  print cUrl 
  print headers
  try:
          #conn.request("GET", "/update", params, headers)
          conn.request("GET",cUrl)
          response = conn.getresponse()
          print response.status, response.reason
          data = response.read()
          conn.close()
  except:
          print "connection failed"
          # todo: collect data if connection failed, send it later
               

