# Distributed with a free-will license.
# Use it any way you want, profit or free, provided it fits in the licenses of its associated works.
# Original code for SI1132
# This code is designed to work with the SI1132_I2CS I2C Mini Module available from ControlEverything.com.
# https://www.controleverything.com/content/Light?sku=SI1132_I2CS#tabs-0-product_tabset-2
#
# 4.4.2017 - KatCe - Going to adapt the code for the SI1145 UV sensor

import smbus
import time

bus = smbus.SMBus(1)

##########
##########
# Sensor Info:
# For details and register addresses, commands,... see datasheet.
# The power consumption is lowest in forced mode.
#
# Typically, after 285 us, AUX_DATA will contain a 16-bit value representing 100 times 
# the sunlight UV Index. Host software must divide the results from AUX_DATA by 100.
#
# Command structure:
# ADR, REGISTER-ADDRESS, Data
##########
##########


# SI1145 address is ADR(96)
ADR = 0x60

# Enable UVindex measurement coefficients
# UCOEF0 (0x13) - UCOEF3 (0x16)
# To To enable UV reading, set the EN_UV bit in CHLIST, and configure UCOEF [3:0] 
# to the default values of 0x29, 0x89, 0x02, and 0x00. 
bus.write_byte_data(ADR, 0x13, 0x29)
bus.write_byte_data(ADR, 0x14, 0x89)
bus.write_byte_data(ADR, 0x15, 0x02)
bus.write_byte_data(ADR, 0x16, 0x00)

# Commented part needs to be done only once (eg. initialization function)

# # The CHLIST Parameter tells the sensor whiche measurements it shall make

# # Select PARAM_WR register, 0x17(23)
		# # 0xF0(15)	Enable UV, Visible, IR
# bus.write_byte_data(ADR, 0x17, 0xF0)
# # Select COMMAND register, 0x18(24)
		# # 0x01 | 0xA0(161)   Select CHLIST register in RAM
# # REGISTER-ADDRESS of CHLIST is 0x01
# bus.write_byte_data(ADR, 0x18, (0x01 | 0xA0))
# time.sleep(0.01)

# # Read data back from 0x2E(46), 1 byte
# response = bus.read_byte_data(ADR, 0x2E)


# # Select INT Output Enable register, 0x03(03)
		# # 0x01(01)	INT pin driven low
# bus.write_byte_data(ADR, 0x03, 0x01)

# # Select ALS Interrupt Enable register, 0x04(04)
		# # 0x01(01)	Assert INT pin whenever VIS or UV measurements are ready
# bus.write_byte_data(ADR, 0x04, 0x01)

# # Select HW_KEY register, 0x07(07)
		# # 0x17(23)	Default value
# bus.write_byte_data(ADR, 0x07, 0x17)


# # Select PARAM_WR register, 0x17(23)
		# # 0x00(0)		Small IR photodiode
# bus.write_byte_data(ADR, 0x17, 0x00)

# # Select COMMAND register, 0x18(24)
		# # 0x0E | 0xA0(174)    Select ALS_IR_ADCMUX register in RAM
# bus.write_byte_data(ADR, 0x18, (0x0E | 0xA0))
# time.sleep(0.01)

# # Read data back from 0x2E(46), 1 byte
# response = bus.read_byte_data(ADR, 0x2E)


# # Select PARAM_WR register, 0x17(23)
		# # 0x00(0)		Set ADC Clock divided / 1
# bus.write_byte_data(ADR, 0x17, 0x00)

# # Select COMMAND register, 0x18(24)
		# # 0x1E | 0xA0(190)    Select ALS_IR_ADC_GAIN register in RAM
# bus.write_byte_data(ADR, 0x18, (0x1E | 0xA0))
# time.sleep(0.01)

# # Read data back from 0x2E(46), 1 byte
# response = bus.read_byte_data(ADR, 0x2E)


# # Select PARAM_WR register, 0x17(23)
		# # 0x70(112)	Set 511 ADC Clock
# bus.write_byte_data(ADR, 0x17, 0x70)

# # Select COMMAND register, 0x18(24)
		# # 0x1D | 0xA0(189)    Select ALS_IR_ADC_COUNTER register in RAM
# bus.write_byte_data(ADR, 0x18, (0x1D | 0xA0))
# time.sleep(0.01)

# # Read data back from 0x2E(46), 1 byte
# response = bus.read_byte_data(ADR, 0x2E)


# # Select PARAM_WR register, 0x17(23)
		# # 0x00(0)		Set ADC Clock divided / 1
# bus.write_byte_data(ADR, 0x17, 0x00)

# # Select COMMAND register, 0x18(24)
		# # 0x11 | 0xA0(177)    Select ALS_VIS_ADC_GAIN register in RAM
# bus.write_byte_data(ADR, 0x18, (0x11 | 0xA0))
# time.sleep(0.01)

# # Read data back from 0x2E(46), 1 byte
# response = bus.read_byte_data(ADR, 0x2E)


# # Select PARAM_WR register, 0x17(23)
		# # 0x20(32)	High Signal Range
# bus.write_byte_data(ADR, 0x17, 0x20)

# # Select COMMAND register, 0x18(24)
		# # 0x1F | 0xA0(191)    Select ALS_IR_ADC_MISC register in RAM
# bus.write_byte_data(ADR, 0x18, (0x1F | 0xA0))
# time.sleep(0.01)

# # Read data back from 0x2E(46), 1 byte
# response = bus.read_byte_data(ADR, 0x2E)


# # Select PARAM_WR register, 0x17(23)
		# # 0x70(112)	Set 511 ADC Clock
# bus.write_byte_data(ADR, 0x17, 0x70)

# # Select COMMAND register, 0x18(24)
		# # 0x10 | 0xA0(176)    Select ALS_VIS_ADC_COUNTER register in RAM
# bus.write_byte_data(ADR, 0x18, (0x10 | 0xA0))
# time.sleep(0.01)

# # Read data back from 0x2E(46), 1 byte
# response = bus.read_byte_data(ADR, 0x2E)


# # Select PARAM_WR register, 0x17(23)
		# # 0x20(32)	High Signal Range
# bus.write_byte_data(ADR, 0x17, 0x20)

# # Select COMMAND register, 0x18(24)
		# # 0x12 | 0xA0(178)    Select ALS_VIS_ADC_MISC register in RAM
# # 0xA0 is PARAM_SET
# bus.write_byte_data(ADR, 0x18, (0x12 | 0xA0))
# time.sleep(0.01)

# # Read data back from 0x2E(46), 1 byte
# response = bus.read_byte_data(ADR, 0x2E)  #0x2E is the response reg PARAM_RD


# Select COMMAND register, 0x18(24)
#		0x0E(14)	Start ALS conversion (# ALS_IR_ADCMUX @ 0x0E)
# PARAM_SET 101 aaaaa dddd 
# dddd

# nnnn nnnn Sets parameter pointed by bitfield 
# [4:0] with value in PARAM_WR, and 
# writes value out to PARAM_RD. See 
# Table 15 for parameters.
bus.write_byte_data(ADR, 0x18, 0x0E)
time.sleep(0.5)


# Read data back from 0x22(34), 4 bytes
# visible lsb, visible msb, ir lsb, ir msb
data = bus.read_i2c_block_data(ADR, 0x22, 4)

# Convert the data
visible = data[1] * 256 + data[0]
ir = data[3] * 256 + data[2]


# Read data back from 0x2C(44), 2 bytes
# uv lsb, uv msb
data = bus.read_i2c_block_data(ADR, 0x2C, 2)

# Convert the data
uv = data[1] * 256 + data[0]
uv = uv/100.0

# Output data to screen
print "Visible Light of Source : %d lux" %visible
print "IR Of Source : %d lux" %ir
print "UV Of the Source : %f lux" %uv
