import RPi.GPIO as GPIO
from time import sleep
from datetime import datetime

# The script as below using BCM GPIO 00..nn numbers

GPIO.setmode(GPIO.BCM)

# Set relay pin as output
GPIO.setup(23, GPIO.OUT)

temp = raw_input()
rh = raw_input()
ldr = raw_input()
ir = raw_input()
uv = raw_input()
    
    # add here additional sensors
    #ir = raw_input()
    #uv = raw_input()
    #visible = raw_input()

# todo: set realistic bounaries for ldr value
if int(ldr) > 900 and (datetime.now().hour >= 6) and (datetime.now().hour<= 21):
    # Turn relay ON
    GPIO.output(23, GPIO.HIGH)
else:
    # Turn all relays OFF
    GPIO.output(23, GPIO.LOW)
