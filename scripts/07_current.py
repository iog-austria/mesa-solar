from ina219 import INA219
from ina219 import DeviceRangeError

SHUNT_OHMS = 0.1

def read():
    ina = INA219(SHUNT_OHMS)
    ina.configure(ina.RANGE_16V)

    try:
        print "%.f" % abs(ina.current())
    except DeviceRangeError as e:
        # Current out of device range with specified shunt resister
        print('Error')


if __name__ == "__main__":
    read()
