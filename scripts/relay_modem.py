# This script is inteded to switch off the internet modem by night.
# The modem has to be connected to a relay, that can be switched off
# by the raspberry pi. The rasperry pi sets the value of the relay
# via an GPIO pin.

import RPi.GPIO as GPIO
from time import sleep
from datetime import datetime

# The script as below using BCM GPIO 00..nn numbers

GPIO.setmode(GPIO.BCM)

# Set relay pin as output
GPIO.setup(23, GPIO.OUT)

temp = raw_input()
rh = raw_input()
ldr = raw_input()
ir = raw_input()
uv = raw_input()
    
    #if a new sensor shall be added to the system, add here additional sensors
    #ir = raw_input()
    #uv = raw_input()
    #visible = raw_input()

#TODO: in GPIO.output set xx to the correct IO PIN number and uncomment the following lines
#      as soon as a second relay is connected in the box

#if (datetime.now().hour >= 6) and (datetime.now().hour<= 21):
#    # Turn relay OFF
#    GPIO.output(xx, GPIO.LOW)
#else:
#    # Turn all relays OFF
#    GPIO.output(xx, GPIO.HIGH)
