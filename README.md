The mesaSolar coding group (mscg) is part of the project mesaSolar by Ingenieure ohne Grenzen Austria (https://www.iog-austria.at/). 

mesaSolar is a PV-driven charging station for mobile phones with free wifi and sitting opportunities. The main goal of mscg is to develop IoT solutions for mesaSolar.

The mesaSolar is a table and bench combination with additional equipment to supply the public with internet, electric power and environmental data. The table was developed together with refugees and ultimately placed in a refugee community project in Traiskirchen, Lower Austria.

Current solutions being worked on (currently on ice in brackets): environmental monitoring, raspberry pi monitoring, (PV system monitoring - state of charge), LED lighting, (wifi usage tracking)

For more information see: 

https://gitlab.com/iog-austria/mesa-solar/tree/master/documentation

https://gitlab.com/iog-austria/mesa-solar/wikis/home