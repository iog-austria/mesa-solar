import owncloud
import glob, os
import config

oc = owncloud.Client('https://owncloud.iog-austria.at/')
oc.login(config.OWNCLOUD_USER, config.OWNCLOUD_PASS)

os.chdir("archive")
for filename in glob.glob("*.csv"):
  local_filesize = int(os.stat(filename).st_size)
  try:
    remote_filesize = int(oc.file_info('sensordata/%s' % filename).get_size())
  except owncloud.owncloud.HTTPResponseError:
    remote_filesize = 0
  if remote_filesize < local_filesize:
    print("%s: remote is smaller than local file, uploading" % filename)
    if oc.put_file('sensordata/%s' % filename, '%s' % filename):
      print("%s was uploaded" % filename)
    else:
      print("%s could not be uploaded" % filename)

